package businessLayer;

import java.io.Serializable;
import java.util.List;

public abstract  class MenuItem implements Serializable {
    /**
	 * 
	 */
	//private static final long serialversionUID =   129348938L;
	private static final long serialVersionUID = -9132974757795579829L;
//	private static final long serialVersionUID  = 1L;
	int id;
    String nume;
    double pret;
   
   public abstract double getPrice();
   public abstract int getId();
   public abstract String getNume();
   public abstract void setNume(String nume);
   public abstract void setPret(double pret);
   public abstract List<MenuItem> getLista();
   
}
