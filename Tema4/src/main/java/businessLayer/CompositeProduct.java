package businessLayer;

import java.io.Serializable;
import java.util.List;

public class CompositeProduct extends MenuItem implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -9132974757795579829L;
	//private static final long serialVersionUID = 1L;
	List<MenuItem> lista;
	public CompositeProduct(int id, String nume, double pret, List<MenuItem> lista) {
		super();
		this.id = id;
		this.nume = nume;
		this.pret = pret;
		this.lista = lista;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public double getPrice() {
		return pret;
	}
	public void setPret(double pret) {
		this.pret=pret;
	}
	
	public List<MenuItem> getLista() {
		return lista;
	}
	public void setLista(List<MenuItem> lista) {
		this.lista = lista;
	}
    public void computePrice() {
    	double pret=0;
    	for(MenuItem i:lista) {
    		 pret+=i.getPrice();		
    	}
    	this.pret=pret;
    }
	
}
