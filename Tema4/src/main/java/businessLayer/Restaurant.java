package businessLayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Observable;

import dataLayer.RestaurantSerializator;

public class Restaurant extends Observable implements RestaurantProcessing{
   ArrayList<MenuItem> listaProduse;
   HashMap<Order,ArrayList<MenuItem>> orders=new HashMap<Order,ArrayList<MenuItem>>();

public Restaurant() {
	super();
	listaProduse=RestaurantSerializator.deserializeMenu();
}

public ArrayList<MenuItem> getListaProduse() {
	return listaProduse;
}

public void setListaProduse(ArrayList<MenuItem> listaProduse) {
	this.listaProduse = listaProduse;
}
   


 public HashMap<Order, ArrayList<MenuItem>> getOrders() {
	return orders;
}

public void setOrders(HashMap<Order, ArrayList<MenuItem>> orders) {
	this.orders = orders;
}

public void addProduct(MenuItem a) {
	assert true;
	int initialSize=listaProduse.size();
	listaProduse.add(a);
	int finalSize=listaProduse.size();
	RestaurantSerializator.serializeMenu(listaProduse);
	assert initialSize==finalSize-1;
 }
 public void deleteProduct(int id) {
	 assert true;
	 int initialSize=listaProduse.size();
	 
	 MenuItem a=null;
	 for(MenuItem i:listaProduse) {
		 if(i.getId()==id) {
			 System.out.println(i.getId()+" "+id);
			 a=i;
			 break;
		 }
	 }
	 if (a!=null) {
		 listaProduse.remove(a);
	 }
	 int finalSize=listaProduse.size();
	 assert initialSize==finalSize+1;	
	 RestaurantSerializator.serializeMenu(listaProduse);
 }
 public void modifyProduct(MenuItem a) {
	 
	 for(MenuItem i:listaProduse) {
		 if(i.getId()==a.getId()) {
			i.setNume(a.getNume());
			i.setPret(a.getPrice());
		 }
		
	}
	 RestaurantSerializator.serializeMenu(listaProduse);
 }
 
 public MenuItem getMenuItemById(int id) {
	 for(MenuItem menu:listaProduse) {
		 if(menu.getId()==id) {
			 return menu;
		 }
	 }
	 return null;
 }
 
 public void addOrder(ArrayList<MenuItem> list,Order order) {
	 assert true;
	 int initialSize=orders.size();
	 orders.put(order, list);
	 int finalSize=orders.size();
	 assert initialSize==finalSize-1;
	 setChanged();
	 notifyObservers(order);
 }
 
 public ArrayList<Order> getJustOrders(){
	 ArrayList<Order> list=new ArrayList<Order>();
	 for(Entry<Order, ArrayList<MenuItem>> entry : orders.entrySet()) {
		 System.out.println(entry.getKey().getOrderId());
		 if(entry.getKey()!=null)
		   list.add(entry.getKey());
	 }
	 
	 return list;
 }
 
 public Order getOrderById(int id) {
	 for(Order or:getJustOrders()) {
		 if(or.getOrderId()==id) {
			 return or;
		 }
	 }
	 return null;
 }
 
 public ArrayList<MenuItem> getProduseByOrder(Order o){
	 return orders.get(o);
 }
 
}
