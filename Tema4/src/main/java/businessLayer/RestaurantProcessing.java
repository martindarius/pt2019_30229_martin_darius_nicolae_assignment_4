package businessLayer;

import java.util.ArrayList;
import java.util.HashMap;

public interface RestaurantProcessing {
	/**
	 * add new menu item
	 * @pre true
	 * @post getSize()==getSize()@pre+1
	 */
	public void addProduct(MenuItem a);
	/**
	 * remove a menu item
	 * @pre getSize()>0 
	 * @post getSize==getSize()@pre-1;
	 */
	public void deleteProduct(int id);
	/**
	 * modify a menu item
	 * @param a the item to be modified
	 * @pre getSize()>0
	 * @post getSize==getSize()@pre
	 */
    public void modifyProduct(MenuItem a);
    /**
     *  return a menu item by id
     * @pre true
     * @post @nochange
     */
    public MenuItem getMenuItemById(int id);
    /**
     * Add an order to HashMap
     * @pre true
     * @post getSize()==getSize()@pre+1
     */
    public void addOrder(ArrayList<MenuItem> list,Order order);
    /**
     * return an ArrayList with menuitems
     * @pre true
     * @post @nochange
     */
    public ArrayList<MenuItem> getListaProduse();
    /**
     * return a hashmap with all orders
     * @pre true
     * @post @nochange
     */
    public HashMap<Order, ArrayList<MenuItem>> getOrders();
    /**
     * return an ArrayList with all orders
     * @pre true
     * @post no change
     */
    public ArrayList<Order> getJustOrders();
    /**
     * Return an order with that id
     * @pre true
     * @post @nochange
     */
    public Order getOrderById(int id);
    /**
     * Return an ArrayList of MenuItem by a specified order
     * @pre true
     * @post @nochange
     */
    public ArrayList<MenuItem> getProduseByOrder(Order o);
}
