package businessLayer;

import java.io.Serializable;
import java.util.List;

public class BaseProduct extends MenuItem implements Serializable {

	/**
	 * 
	 */
//
	//private static final long serialVersionUID = 1L;
	private static final long serialVersionUID = -9132974757795579829L;
	public BaseProduct(int id,String nume,double pret) {
		super();
		this.id=id;
		this.nume=nume;
		this.pret=pret;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public double getPrice() {
		return pret;
	}

	public void computePrice(double pret) {
		this.pret = pret;
	}

	public void setPret(double pret) {
		this.pret = pret;
	}
	public List<MenuItem> getLista(){
		return null;
	}
	
}
