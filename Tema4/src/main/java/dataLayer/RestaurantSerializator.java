package dataLayer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import businessLayer.MenuItem;

public class RestaurantSerializator {

	public static void serializeMenu(ArrayList<MenuItem> lista) {
		try {
			 FileOutputStream file = new FileOutputStream("RestaurantSerializator.ser"); 
	         ObjectOutputStream out = new ObjectOutputStream(file);	
	         out.writeInt(lista.size());
	         if(lista.size()!=0) {
	        	for(MenuItem i:lista) {
	        		out.writeObject(i);
	        	}
	         }
	      //  out.writeObject(lista);
	        out.close();
	        file.close();
		} catch(IOException ex) {
			System.out.println("IOException is caught");
			System.out.println("serializare");
		}
	}
	
	
	public static ArrayList<MenuItem> deserializeMenu(){
		MenuItem item=null;
		ArrayList<MenuItem> lista=new ArrayList<MenuItem>();
		int size=0;
		try {
			FileInputStream file = new FileInputStream("RestaurantSerializator.ser"); 
            ObjectInputStream in = new ObjectInputStream(file); 
            try {
            size=in.readInt();
            }catch(IOException err) {
            	size=0;
            }
            System.out.println("Meniul are "+size+" iteme");
            for(int i=0;i<size;i++) {
            	
            	item=(MenuItem)in.readObject();
            	lista.add(item);
            	
            }			
         //  lista= (ArrayList<MenuItem>) in.readObject();
           in.close();
           file.close();
 		}catch(IOException ex) 
        { 
            System.out.println("IOException is caught"); 
            System.out.println("Deserializare");
            ex.printStackTrace();
        } 
          
        catch(ClassNotFoundException ex) 
        { 
            System.out.println("ClassNotFoundException is caught"); 
        } 
		return lista;
	}
}
