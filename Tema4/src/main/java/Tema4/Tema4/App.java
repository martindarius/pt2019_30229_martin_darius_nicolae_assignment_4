package Tema4.Tema4;

import businessLayer.Restaurant;
import presentationLayer.MainInterface;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	Restaurant res=new Restaurant();
        MainInterface view=new MainInterface(res);
        res.addObserver(view.getY());
    }
}
