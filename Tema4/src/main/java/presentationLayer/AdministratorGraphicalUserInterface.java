package presentationLayer;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.RestaurantProcessing;

public class AdministratorGraphicalUserInterface {
	JFrame frame = new JFrame("Administrator:");
	JFrame basic = new JFrame("Base product:");
	JFrame composite = new JFrame("Composite product:");

	JButton basicButton = new JButton("Basic product");
	JButton compositeButton = new JButton("Composite product");

	JButton addbasic = new JButton("ADD PRODUCT");
	JButton deletebasic = new JButton("DELETE PRODUCT");
	JButton updatebasic = new JButton("UPDATE PRODUCT");

	JButton addcomposite = new JButton("ADD PRODUCT");
	JButton deletecomposite = new JButton("DELETE PRODUCT");
	JButton updatecomposite = new JButton("UPDATE PRODUCT");
	JButton viewAll1 = new JButton("View List");
	JButton viewAll2 = new JButton("View List");

	JLabel numeBasic = new JLabel("NUME:");
	JLabel idBasic = new JLabel("ID:");
	JLabel pretBasic = new JLabel("PRET:");

	JTextField numeBasicText = new JTextField(30);
	JTextField idBasicText = new JTextField(5);
	JTextField pretBasicText = new JTextField(5);

	JLabel numeComposite = new JLabel("NUME:");
	JLabel idComposite = new JLabel("ID:");
	JLabel listaComposite = new JLabel("Lista produse:");
	JLabel pretComposite = new JLabel("Pret:");

	JTextField numeCompositeText = new JTextField(30);
	JTextField idCompositeText = new JTextField(5);
	JTextField listaCompositeText = new JTextField(20);
	JTextField pretCompositeText = new JTextField(5);

	final RestaurantProcessing res;

	public AdministratorGraphicalUserInterface(final RestaurantProcessing res) {
		this.res = res;

		JPanel p = new JPanel();
		p.add(basicButton);
		p.add(compositeButton);
		p.setLayout(new FlowLayout());
		frame.add(p);
		frame.setSize(400, 100);
		frame.setLocation(300, 200);

		// frame.setVisible(true);

		JPanel p1 = new JPanel();
		JPanel p11 = new JPanel();
		p1.add(numeBasic);
		p1.add(numeBasicText);
		p1.add(idBasic);
		p1.add(idBasicText);
		p1.add(pretBasic);
		p1.add(pretBasicText);
		p1.setLayout(new GridLayout(3, 2));
		p11.add(addbasic);
		p11.add(deletebasic);
		p11.add(updatebasic);

		p11.add(viewAll1);
		p11.setLayout(new FlowLayout());
		basic.add(p1, BorderLayout.PAGE_START);
		basic.add(p11, BorderLayout.PAGE_END);
		basic.setSize(600, 300);
		basic.setLocation(300, 350);

		basicButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				basic.setVisible(true);
			}

		});

		JPanel p2 = new JPanel();
		JPanel p22 = new JPanel();

		p2.add(numeComposite);
		p2.add(numeCompositeText);
		p2.add(idComposite);
		p2.add(idCompositeText);
		p2.add(listaComposite);
		p2.add(listaCompositeText);
		p2.add(pretComposite);
		p2.add(pretCompositeText);
		p2.setLayout(new GridLayout(4, 2));
		p22.add(addcomposite);
		p22.add(deletecomposite);
		p22.add(updatecomposite);
		p22.add(viewAll2);
		p22.setLayout(new FlowLayout());
		composite.add(p2, BorderLayout.PAGE_START);
		composite.add(p22, BorderLayout.PAGE_END);
		composite.setSize(600, 300);
		composite.setLocation(300, 350);

		compositeButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				composite.setVisible(true);
			}

		});

		addbasic.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String idtext = "", nume = "", prettext = "";
				int id;
				double pret;
				idtext = idBasicText.getText();
				nume = numeBasicText.getText();
				prettext = pretBasicText.getText();
				if (!((idtext.equals("")) || (nume.equals("")) || (prettext.equals("")))) {
					id = Integer.parseInt(idtext);
					pret = Double.parseDouble(prettext);
					ArrayList<MenuItem> lista = res.getListaProduse();
					int ok = 1;
					for (MenuItem item : lista) {
						if (item.getId() == id) {
							ok = 0;
						}
					}
					if (ok == 1) {
						BaseProduct a = new BaseProduct(id, nume, pret);
						res.addProduct(a);
					} else {
						JOptionPane.showMessageDialog(frame, "ID ul deja exista!");
					}
				} else {
					JOptionPane.showMessageDialog(frame, "Va rog completati toate campurile.");
				}
			}

		});

		updatebasic.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String idtext = "", nume = "", prettext = "";
				int id;
				double pret;
				idtext = idBasicText.getText();
				nume = numeBasicText.getText();
				prettext = pretBasicText.getText();
				if (!((idtext.equals("")) || (nume.equals("")) || (prettext.equals("")))) {
					id = Integer.parseInt(idtext);
					pret = Double.parseDouble(prettext);
					ArrayList<MenuItem> lista = res.getListaProduse();
					int ok = 1;
					for (MenuItem item : lista) {
						if (item.getId() == id) {
							ok = 0;
						}
					}
					if (ok == 0) {
						BaseProduct a = new BaseProduct(id, nume, pret);
						res.modifyProduct(a);
					} else {
						JOptionPane.showMessageDialog(frame, "ID ul nu exista!");
					}
				} else {
					JOptionPane.showMessageDialog(frame, "Va rog completati toate campurile.");
				}
			}

		});

		deletebasic.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String idtext = "";
				int id;

				idtext = idBasicText.getText();
				if (!idtext.equals("")) {
					id = Integer.parseInt(idtext);
					ArrayList<MenuItem> lista = res.getListaProduse();
					int ok = 1;
					for (MenuItem item : lista) {
						if (item.getId() == id) {
							ok = 0;
						}
					}
					if (ok == 0) {
						res.deleteProduct(id);
					} else {
						JOptionPane.showMessageDialog(frame, "ID ul nu exista!");
					}

				} else {
					JOptionPane.showMessageDialog(frame, "Va rog completati ID-ul.");
				}
			}

		});

		addcomposite.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String idtext = "", nume = "", listatext = "";
				ArrayList<MenuItem> lista = new ArrayList<MenuItem>();
				int id;
				idtext = idCompositeText.getText();
				nume = numeCompositeText.getText();
				listatext = listaCompositeText.getText();
				if (!((idtext.equals("")) || (nume.equals("")) || (listatext.equals("")))) {
					id = Integer.parseInt(idtext);
					String[] lista2 = listatext.split(" ");
					for (String a : lista2) {
						int x = Integer.parseInt(a);
						MenuItem produs = res.getMenuItemById(x);
						if (produs == null) {
							showException("ID ul " + x + " nu exista!");
						} else {
							lista.add(produs);
						}
					}
					ArrayList<MenuItem> lista3 = res.getListaProduse();
					int ok = 1;
					for (MenuItem item : lista3) {
						if (item.getId() == id) {
							ok = 0;
						}
					}
					if (ok == 1) {
						CompositeProduct a = new CompositeProduct(id, nume, 0, lista);
						a.computePrice();
						res.addProduct(a);
					} else {
						JOptionPane.showMessageDialog(frame, "ID ul deja exista!");
					}
				} else {
					JOptionPane.showMessageDialog(frame, "Va rog completati toate campurile!");
				}
			}

		});

		updatecomposite.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String idtext = "", nume = "", prettext = "";
				int id;
				double pret;
				ArrayList<MenuItem> list = new ArrayList<MenuItem>();
				idtext = idCompositeText.getText();
				nume = numeCompositeText.getText();
				prettext = pretCompositeText.getText();
				if (!((idtext.equals("")) || (nume.equals("")) || (prettext.equals("")))) {
					id = Integer.parseInt(idtext);
					pret = Double.parseDouble(prettext);
					int ok = 1;
					ArrayList<MenuItem> lista3 = res.getListaProduse();
					for (MenuItem item2 : lista3) {
						if (item2.getId() == id) {
							ok = 0;
						}
					}
					if (ok == 0) {
						MenuItem item = res.getMenuItemById(id);
						list = (ArrayList<MenuItem>) item.getLista();
						CompositeProduct a = new CompositeProduct(id, nume, pret, list);
						res.modifyProduct(a);
					} else {
						JOptionPane.showMessageDialog(frame, "ID ul nu exista!");
					}
				} else {
					JOptionPane.showMessageDialog(frame, "Va rog completati toate campurile.");
				}

			}

		});

		deletecomposite.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String idtext = "";
				int id;

				idtext = idCompositeText.getText();
				if (!idtext.equals("")) {
					id = Integer.parseInt(idtext);
					int ok = 1;
					ArrayList<MenuItem> lista = res.getListaProduse();
					for (MenuItem item : lista) {
						if (item.getId() == id) {
							ok = 0;
						}
					}
					if (ok == 0) {
						res.deleteProduct(id);
					} else {
						JOptionPane.showMessageDialog(frame, "ID ul nu exista!");
					}
				} else {
					JOptionPane.showMessageDialog(frame, "Va rog completati ID ul");
				}

			}

		});

		viewAll1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				JFrame f = new JFrame();
				f.setTitle("Meniu:");
				String[] columnsName = { "ID", "NUME", "PRET" };
				ArrayList<MenuItem> lista = res.getListaProduse();
				int s = lista.size();
				String[][] data = new String[s][3];
				int j = 0;
				for (MenuItem i : lista) {
					int x = i.getId();
					double y = i.getPrice();
					data[j][0] = Integer.toString(x);
					data[j][1] = i.getNume();
					data[j][2] = Double.toString(y);
					j++;
				}
				JTable table = new JTable(data, columnsName);
				JScrollPane sp = new JScrollPane(table);
				f.add(sp);
				f.setSize(500, 200);
				f.setVisible(true);
			}

		});

		viewAll2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				JFrame f = new JFrame();
				f.setTitle("Meniu:");
				String[] columnsName = { "ID", "NUME", "PRET" };
				ArrayList<MenuItem> lista = res.getListaProduse();
				int s = lista.size();
				String[][] data = new String[s][3];
				int j = 0;
				for (MenuItem i : lista) {
					int x = i.getId();
					double y = i.getPrice();
					data[j][0] = Integer.toString(x);
					data[j][1] = i.getNume();
					data[j][2] = Double.toString(y);
					j++;
				}
				JTable table = new JTable(data, columnsName);
				JScrollPane sp = new JScrollPane(table);
				f.add(sp);
				f.setSize(500, 200);
				f.setVisible(true);
			}

		});

	}

	public String getbasicID() {
		return idBasicText.getText();
	}

	public String getbasicName() {
		return numeBasicText.getText();
	}

	public String getbasicPrice() {
		return pretBasicText.getText();
	}

	public String getCompositeID() {
		return idCompositeText.getText();
	}

	public String getCompositeName() {
		return numeCompositeText.getText();
	}

	public String getCompositeList() {
		return listaCompositeText.getText();
	}

	public String getCompositePret() {
		return pretCompositeText.getText();
	}

	public void showException(String mesaj) {
		JOptionPane.showMessageDialog(frame, mesaj);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

}
