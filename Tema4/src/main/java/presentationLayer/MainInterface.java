package presentationLayer;

import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import businessLayer.Restaurant;
import businessLayer.RestaurantProcessing;

public class MainInterface {
	JFrame principal = new JFrame("Principal");
	JButton admin=new JButton("Administrator");
	JButton chef=new JButton("Chef");
	JButton waiter=new JButton("Waiter");
	final RestaurantProcessing res;
	AdministratorGraphicalUserInterface x;
	ChefGraphicalUserInterface y;
	WaiterGraphicalUserInterface z;
	
	
	public MainInterface(final RestaurantProcessing res) {
		this.res=res;
		x=new AdministratorGraphicalUserInterface(res);
	    y=new ChefGraphicalUserInterface(res);
		z=new WaiterGraphicalUserInterface(res);
		Panel c1=new Panel();
		c1.add(admin);
		c1.add(chef);
		c1.add(waiter);
		c1.setLayout(new FlowLayout());
	    principal.add(c1);
	    principal.setSize(300, 100);
	    principal.setLocation(100, 100);
	    principal.setVisible(true);
	    
	    admin.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				x.getFrame().setVisible(true);
			}
	    	
	    });
	    
	    chef.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				y.getChef().setVisible(true);
			}
	    	
	    });

	    waiter.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				z.getFrame().setVisible(true);
			}
	    	
	    });
	    

	    
	}


	public ChefGraphicalUserInterface getY() {
		return y;
	}


	public void setY(ChefGraphicalUserInterface y) {
		this.y = y;
	}
	
	
}
