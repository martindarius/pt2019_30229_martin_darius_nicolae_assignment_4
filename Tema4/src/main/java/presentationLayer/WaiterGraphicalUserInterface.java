package presentationLayer;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.RestaurantProcessing;

public class WaiterGraphicalUserInterface {
	JFrame frame = new JFrame("Waiter:");
	JFrame ordersTable=new JFrame("Tabel of orders:");
	
	JButton generateBill=new JButton("Generate Bill");
	JButton computePrice=new JButton("Compute Price");
	JButton addToOrder=new JButton("Add To Order");
	JButton viewAllOrders=new JButton("ViewAllOrders");
	
	JLabel orderId=new JLabel("OrderId:");
	JLabel productId=new JLabel("Products Id:");
	JLabel table=new JLabel("Table:");
	JTextField orderIdText=new JTextField(5);
	JTextField productIdText=new JTextField(20);
	JTextField tableText=new JTextField(5);
	
	JTextArea text=new JTextArea(150,200);
	
	final RestaurantProcessing res;
	
	HashMap<Integer,Double> preturi=new HashMap<Integer,Double>();
	
	
	public WaiterGraphicalUserInterface(final RestaurantProcessing res) {
		this.res=res;
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		
		p1.add(addToOrder);
		p1.add(computePrice);
		p1.add(viewAllOrders);
		p1.add(generateBill);
		p1.setLayout(new FlowLayout());
		p2.add(orderId);
		p2.add(orderIdText);
		p2.add(productId);
		p2.add(productIdText);
		p2.add(table);
		p2.add(tableText);
		p2.setLayout(new GridLayout(3,2));
		frame.add(p1,BorderLayout.PAGE_END);
		frame.add(text,BorderLayout.CENTER );
		frame.add(p2, BorderLayout.PAGE_START);
		frame.setSize(500, 300);
		frame.setLocation(600, 600);
		
		//frame.setVisible(true);
		
		
		addToOrder.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
                  String table="",orderId="",lista="";
                  ArrayList<MenuItem> listaProduse=new ArrayList<MenuItem>();
                  int t,i;
                  double pret=0;
                  Date date;
                  table=tableText.getText();
                  orderId=orderIdText.getText();
                  lista=productIdText.getText();
                  
                  i=Integer.parseInt(orderId);
                  t=Integer.parseInt(table);
                  date=new Date();
                  Order o=new Order(i,date,t);
                  String[] lista2=lista.split(" ");
  				  for(String a:lista2) {
  					int x=Integer.parseInt(a);
  					//System.out.println(x);
  					MenuItem produs = res.getMenuItemById(x);
  					if (produs == null) {
  					   showException("ID ul "+x+" nu exista!");
  					}else {
  						listaProduse.add(produs);
  						pret+=produs.getPrice();
  						
  					}
  				} 
  				  
                  res.addOrder(listaProduse,o);
                  preturi.put(o.getOrderId(), pret);
                  System.out.println(preturi);
			}
			
		});
		
		viewAllOrders.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				JFrame f=new JFrame();
				f.setTitle("Orders:");
				String[] columnsName= {"ID","DATA"};
				ArrayList<Order> lista=res.getJustOrders();
				int s=0;
				String[][]data=null;
				if(lista!=null) {
			    s=lista.size();
			    int j=0;
			    data=new String[s][2];
				for(Order i:lista) {
					int x=i.getOrderId();
					Date date=i.getDate();
					data[j][0]=Integer.toString(x);
					data[j][1]=date.toString();
					j++;
				}
				}
				JTable table=new JTable(data,columnsName);
				JScrollPane sp = new JScrollPane(table); 
		        f.add(sp);  
		        f.setSize(500, 200);  
		        f.setVisible(true); 
			}
			
		});
		
		computePrice.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
			String idOrder="";
			int id;
			idOrder=orderIdText.getText();
			id=Integer.parseInt(idOrder);
			//text.setText(preturi.toString());
			text.setText("Comanda cu id-ul: "+idOrder+" are pretul total: "+preturi.get(id));
			}
		});
		
		generateBill.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				String idtext="";
				idtext=orderIdText.getText();
				int id;
				id=Integer.parseInt(idtext);
				Order o=res.getOrderById(id);
				ArrayList<MenuItem> produse=new ArrayList<MenuItem>();
				produse=res.getProduseByOrder(o);
				try {
					PrintWriter fisierTxt=new PrintWriter("Factura.txt","UTF-8");
					fisierTxt.println("Factura:");
					fisierTxt.println("La masa "+o.getTable()+" s-a emis o factura cu ID-ul:"+id+" in valoare de "+preturi.get(id));
					fisierTxt.println("Comanda contine:");
					for(MenuItem i:produse) {
						fisierTxt.println(i.getNume()+" "+i.getPrice());
					}
					
					
					
				    fisierTxt.close();	
				}catch(Exception e) {
					System.out.println(e.getMessage()+" la scriere in fisier");
				}
			}
			
		});
	}
	
	
	public void showException(String mesaj) {
		JOptionPane.showMessageDialog(frame,mesaj);
	}


	public JFrame getFrame() {
		return frame;
	}


	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	
	
	
	
}
