package presentationLayer;

import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import businessLayer.RestaurantProcessing;

public class ChefGraphicalUserInterface implements Observer{
    JFrame chef=new JFrame("Chef:");   
	JTextArea text=new JTextArea(30,30);
    JLabel comanda=new JLabel("Comanda este:");  
    RestaurantProcessing res;
      
    public ChefGraphicalUserInterface(final RestaurantProcessing  res) {
    		this.res = res;
    	   JPanel c1=new JPanel();
    	   c1.add(comanda);
    	   c1.add(text);
    	   text.setEditable(false);
    	   c1.setLayout(new FlowLayout());
    	   chef.add(c1);
    	   chef.setSize(400,300);
    	   chef.setLocation(400,500);
    	   
    	   //chef.setVisible(true);
       }

	public void update(Observable arg0, Object o) {
		
		Order a=(Order)o;
		Order or=res.getOrderById(a.getOrderId());
		ArrayList<MenuItem>produse=new ArrayList<MenuItem>();
		produse=res.getProduseByOrder(or);
		System.out.println("Fdwf");
		this.text.setText("Comanda cu id-ul "+a.getOrderId()+" de la masa "+a.getTable()+" contine:");
		for(MenuItem i:produse) {
			text.append(i.getNume()+" \n");	
		}
		
	}

	public JFrame getChef() {
		return chef;
	}

	public void setChef(JFrame chef) {
		this.chef = chef;
	}
	
}
